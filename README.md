Introduction
============

[![npm version](https://img.shields.io/npm/v/admin-lte.svg)](https://www.npmjs.com/package/admin-lte)

**react-adminlte** -- is a React version of fully responsive admin template AdminLTE. Based on **[Bootstrap 3](https://github.com/twbs/bootstrap)** framework. Highly customizable and easy to use. Fits many screen resolutions from small mobile devices to large desktops. Check out the live preview now and see for yourself.

**Preview Admin LTE features on [AdminLTE.IO](https://adminlte.io)**


## Documentation & Installation Guide
Visit the [online code generator](https://react-adminlte.xaifiet.com/) for the most updated guide.
