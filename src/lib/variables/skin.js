export const Skins = [
    'default',
    'primary',
    'info',
    'success',
    'warning',
    'danger',
];