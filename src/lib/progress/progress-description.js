import React from 'react';

export const ProgressDescription = function (props) {
    return (
        <span className="progress-description">
            {props.children}
        </span>
    );
};
