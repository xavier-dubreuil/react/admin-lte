import React from 'react';
import PropTypes from 'prop-types';

export const ProgressBar = function (props) {

    let classes = ['progress'];
    if (props.vertical) {
        classes.push('vertical');
    }
    if (props.active) {
        classes.push('active');
    }
    if (props.size) {
        classes.push('progress-' + props.size);
    }

    let classesProgress = ['progress-bar'];
    if (props.skin) {
        classesProgress.push('progress-bar-' + props.skin);
    }
    if (props.striped) {
        classesProgress.push('progress-bar-striped');
    }

    let style = props.vertical ? {height: props.percent + '%'} : {width: props.percent + '%'};

    return (
        <div className={classes.join(' ')}>
            <div className={classesProgress.join(' ')} role="progressbar" aria-valuenow={props.percent}
                aria-valuemin="0" aria-valuemax="100" style={style}>
            </div>
        </div>
    );
};

ProgressBar.defaultProps = {
    vertical: false,
    striped: false,
    active: false,
};

ProgressBar.propTypes = {
    skin: PropTypes.string,
    percent: PropTypes.number,
    vertical: PropTypes.bool,
    striped: PropTypes.bool,
    active: PropTypes.bool,
    size: PropTypes.string,
};