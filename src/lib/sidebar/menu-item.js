import React from 'react';
import PropTypes from 'prop-types';
import {withRouter, Link} from 'react-router-dom';

const LinkItem = function(props) {

    if (props.to && props.to !== '#') {
        return (
            <Link to={props.to}>{props.children}</Link>
        );
    }
    return (
        <a onClick={props.onClick}>{props.children}</a>
    );
};

LinkItem.propTypes = {
    to: PropTypes.string,
    onClick: PropTypes.func,
};


class SidebarMenuItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: this.childrenActive(props.children),
        };
    }

    toggleOpen() {
        if (this.props.children) {
            this.setState({
                open: !this.state.open,
            });
        }
    }

    childrenActive(children) {
        if (children === undefined) {
            return false;
        }
        if (!Array.isArray(this.props.children)) {
            children = [children];
        }
        for (let i = 0; i < children.length; i++) {
            if (children.hasOwnProperty(i)) {
                if (children[i].props.to === this.props.history.location.pathname) {
                    return true;
                }
            }
            if (this.childrenActive(children[i].props.children)) {
                return true;
            }
        }
        return false;
    }

    render() {
        let children = [];
        if (Array.isArray(this.props.children)) {
            children = this.props.children;
        } else if (this.props.children !== undefined) {
            children = [this.props.children];
        }

        let classes = ['treeview'];
        if (this.state.open) {
            classes.push('menu-open');
        }
        if (this.props.history.location.pathname === this.props.to || this.childrenActive(this.props.children)) {
            classes.push('active');
        }

        let iconClasses = ['fa'];
        if (this.props.icon) {
            iconClasses.push('fa-' + this.props.icon);
            if (this.props.iconColor) {
                iconClasses.push('text-' + this.props.iconColor);
            }
        }

        return (
            <li className={classes.join(' ')}>
                <LinkItem to={this.props.to ? this.props.to : '#'} onClick={this.toggleOpen.bind(this)}>
                    {this.props.icon && <i className={iconClasses.join(' ')}/>}
                    <span>{this.props.title}</span>

                    <span className="pull-right-container">
                        {this.props.labels && this.props.labels.map((label) => {
                            return (
                                <small key={label.title}
                                    className={'label pull-right bg-' + label.color}>{label.title}</small>
                            );
                        })}
                        {!this.props.labels && children.length > 0 &&
                        <i className="fa fa-angle-left pull-right"/>
                        }
                    </span>
                </LinkItem>
                {children.length > 0 &&
                <ul className="treeview-menu" style={{display: this.state.open ? 'block' : 'none'}}>
                    {children.map((item) => {
                        return item;
                    })}
                </ul>
                }
            </li>
        );
    }
}

SidebarMenuItem.defaultProps = {
    icon: 'circle-o',
};

SidebarMenuItem.propTypes = {
    title: PropTypes.string.isRequired,
    to: PropTypes.string,
    icon: PropTypes.string,
    iconColor: PropTypes.string,
    labels: PropTypes.array,
};

SidebarMenuItem.contextTypes = {
    router: PropTypes.shape({
        history: PropTypes.object.isRequired,
    }),
};

export default withRouter(SidebarMenuItem);
