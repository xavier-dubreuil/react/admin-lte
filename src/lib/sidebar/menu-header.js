import React from 'react';
import PropTypes from 'prop-types';

export const SidebarMenuHeader = function(props) {

    return (
        <li className="header">{props.title}</li>
    );
};

SidebarMenuHeader.propTypes = {
    title: PropTypes.string.isRequired,
};
