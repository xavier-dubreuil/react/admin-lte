import React from 'react';

export const SidebarMenu = function(props) {

    return (
        <ul className="sidebar-menu tree" data-widget="tree">
            {props.children}
        </ul>
    );
};
