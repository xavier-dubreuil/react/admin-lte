import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export class NavbarMenuItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
        };
    }

    toggleOpen() {
        this.setState({
            open: !this.state.open,
        });
    }

    render() {
        if (!this.props.children) {
            if (this.props.to) {
                return (
                    <li>
                        <Link to={this.props.to}>{this.props.title}</Link>
                    </li>
                );
            } else if (this.props.onClick) {
                return (
                    <li>
                        <a onClick={this.props.onClick}>{this.props.title}</a>
                    </li>
                );
            }
        }
        let classes = ['dropdown'];
        if (this.state.open) {
            classes.push('open');
        }
        return (
            <li className={classes.join(' ')}>
                <a onClick={this.toggleOpen.bind(this)}>{this.props.title}</a>
                {this.state.open &&
                    <ul className="dropdown-menu">
                        {this.props.children}
                    </ul>
                }
            </li>
        );
    }
}

NavbarMenuItem.propTypes = {
    title: PropTypes.any.isRequired,
    to: PropTypes.string,
};
