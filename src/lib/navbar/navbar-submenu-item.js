import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export const NavbarSubmenuItem = function(props) {
    return (
        <li>
            <Link to={props.to}>
                {props.children}
            </Link>
        </li>
    );
};

NavbarSubmenuItem.propTypes = {
    to: PropTypes.string.isRequired,
};
