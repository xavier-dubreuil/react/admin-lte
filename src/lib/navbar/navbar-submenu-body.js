import React from 'react';

export const NavbarSubmenuBody = function (props) {

    return (
        <li>
            <ul className="menu">
                {props.children}
            </ul>
        </li>
    );
};
