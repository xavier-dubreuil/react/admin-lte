import React from 'react';

export const NavbarMenu = function(props) {
    return (
        <div className="navbar-custom-menu">
            <ul className="nav navbar-nav">
                {props.children}
            </ul>
        </div>
    );
};
