import React from 'react';

export const NavbarSubmenuHeader = function(props) {
    return (
        <li className="header">{props.children}</li>
    );
};
