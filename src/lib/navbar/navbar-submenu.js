import React from 'react';

export const NavbarSubmenu = function(props) {
    return (
        <ul className="dropdown-menu">
            {props.children}
        </ul>
    );
};
