import React from 'react';

export const NavbarSubmenuFooter = function(props) {

    return (
        <li className="footer">{props.children}</li>
    );
};
