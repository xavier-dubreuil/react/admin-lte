import React from 'react';
import PropTypes from 'prop-types';

export const ChatMessage = function(props) {

    let classes = ['direct-chat-msg'];
    if (props.right) {
        classes.push('right');
    }

    return (
        <div className={classes.join(' ')}>
            <div className="direct-chat-info clearfix">
                <span className="direct-chat-name pull-left">{props.name}</span>
                <span className="direct-chat-timestamp pull-right">{props.time}</span>
            </div>
            <img className="direct-chat-img" src={props.imgsrc} alt="Message User"/>
            <div className="direct-chat-text">
                {props.children}
            </div>
        </div>
    );
};

ChatMessage.propTypes = {
    name: PropTypes.string,
    time: PropTypes.string,
    imgsrc: PropTypes.string,
    right: PropTypes.bool
};