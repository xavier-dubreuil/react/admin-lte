import React from 'react';
import PropTypes from 'prop-types';

export const Chat = function(props) {

    let classes = ['direct-chat-messages'];
    if (props.color) {
        classes.push('direct-chat-' + props.color);
    }

    return (
        <div className={classes.join(' ')}>
            {props.children}
        </div>
    );
};

Chat.propTypes = {
    color: PropTypes.string,
};