import React from 'react';
import PropTypes from 'prop-types';

export const Callout = function(props) {

    let classes = ['callout', 'callout-'+props.skin];
    if (props.className !== '') {
        classes.push(props.className);
    }

    return (
        <div className={classes.join(' ')}>
            {props.children}
        </div>
    );
};

Callout.api = {
    className: {
        type: 'String',
        default: '',
        desc: 'Extra CSS classes',
        isRequired: false,
    },
};

Callout.defaultProps = {
    className: '',
};

Callout.propTypes = {
    skin: PropTypes.string.isRequired,
    className: PropTypes.string,
};
