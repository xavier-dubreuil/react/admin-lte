import React from 'react';
import PropTypes from 'prop-types';

export const ButtonGroup = function(props) {
    let className = props.className.split(' ');
    className.push(props.vertical ? 'btn-group-vertical' : 'btn-group');

    return (
        <div className={className.join(' ')}>
            {props.children}
        </div>
    );
};

ButtonGroup.defaultProps = {
    className: '',
    vertical: false,
};

ButtonGroup.propTypes = {
    vertical: PropTypes.bool,
};
