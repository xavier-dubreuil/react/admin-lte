import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export const ButtonApp = function(props) {

    let classes = props.extraClass.split(' ');
    classes.push('btn');
    classes.push('btn-app');

    return (
        <Link className={classes.join(' ')} to={props.to}>
            {props.badge &&
                <span className={'badge bg-'+props.badgeColor}>{props.badge}</span>
            }
            {props.children}
        </Link>
    );
};

ButtonApp.defaultProps = {
    extraClass: '',
};

ButtonApp.propTypes = {
    extraClass: PropTypes.string,
    badge: PropTypes.string,
    badgeColor: PropTypes.string,
    to: PropTypes.string.isRequired,
};
