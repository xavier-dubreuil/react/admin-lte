import React from 'react';
import PropTypes from 'prop-types';

export const Button = function(props) {

    let classes = props.extraClass.split(' ');
    classes.push('btn');
    if (props.size !== 'normal') {
        classes.push('btn-' + props.size);
    }
    if (props.skin) {
        classes.push('btn-' + props.skin);
    }
    if (props.color) {
        classes.push('bg-' + props.color);
    }
    if (props.flat) {
        classes.push('btn-flat');
    }
    if (props.disabled) {
        classes.push('disabled');
    }
    if (props.block) {
        classes.push('btn-block');
    }
    if (props.margin) {
        classes.push('margin');
    }

    return (
        <button type="button" className={classes.join(' ')} onClick={props.onClick}>{props.children}</button>
    );
};

Button.defaultProps = {
    extraClass: '',
    size: 'normal',
    flat: false,
    disabled: false,
    block: false,
    margin: false,
};

Button.propTypes = {
    extraClass: PropTypes.string,
    size: PropTypes.string,
    skin: PropTypes.string,
    color: PropTypes.string,
    flat: PropTypes.bool,
    disabled: PropTypes.bool,
    block: PropTypes.bool,
    margin: PropTypes.bool,
    onClick: PropTypes.func,
};
