import React from 'react';

export const FormGroup = function (props) {
    return (
        <div className="form-group">
            {props.children}
        </div>
    );
};
