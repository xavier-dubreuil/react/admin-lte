import React from 'react';
import PropTypes from 'prop-types';
import {Input} from './input';

export const FormCheckbox = function (props) {
    let classname = 'checkbox';
    let classHelp = 'help-block';
    if (props.error) {
        classname += ' has-error';
    }
    if (props.horizontal) {
        classname += ' col-sm-offset-2 col-sm-10';
        classHelp += ' col-sm-offset-2 col-sm-10';
    }

    return (
        <div className={classname}>
            <label>
                <Input {...props} type="checkbox" />
                {props.label}
            </label>
            {props.help &&
                <span className={classHelp}>{props.help}</span>
            }
        </div>
    );
};

FormCheckbox.defaultProps = {
    horizontal: false,
    error: false,
};

FormCheckbox.propTypes = {
    horizontal: PropTypes.bool,
    type: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    extraClass: PropTypes.string,
    handleChange: PropTypes.func,
    disabled: PropTypes.bool,
    label: PropTypes.any,
    value: PropTypes.any,
    error: PropTypes.bool,
    help: PropTypes.any,
};
