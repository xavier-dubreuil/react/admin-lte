import React from 'react';
import PropTypes from 'prop-types';

export const FormControl = function (props) {
    let classes = ['form-group'];
    if (props.error) {
        classes.push('has-error');
    }


    let classHelp = ['help-block'];
    let classLabel = ['control-label'];
    if (props.horizontal) {
        classLabel.push('col-sm-2');
        classHelp.push('col-sm-offset-2 col-sm-10');
    }

    return (
        <div className={classes.join(' ')}>
            <label htmlFor={props.id} className={classLabel.join(' ')}>{props.label}</label>
            {props.horizontal &&
                <div className="col-sm-10">
                    {props.children}
                </div>
            }
            {!props.horizontal && props.children}
            {props.help &&
                <span className={classHelp.join(' ')}>{props.help}</span>
            }
        </div>
    );
};

FormControl.defaultProps = {
    className: '',
    horizontal: false,
    error: false,
};

FormControl.propTypes = {
    horizontal: PropTypes.bool,
    label: PropTypes.any,
    error: PropTypes.bool,
    help: PropTypes.any,
};
