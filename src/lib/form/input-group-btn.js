import React from 'react';
import PropTypes from 'prop-types';

export const InputGroupButton = function(props) {
    let className = props.className.split(' ');
    className.push('input-group-btn');
    if (props.margin) {
        className.push('margin');
    }

    return (
        <div className={className.join(' ')}>
            {props.children}
        </div>
    );
};

InputGroupButton.defaultProps = {
    className: '',
};

InputGroupButton.propTypes = {
    margin: PropTypes.bool,
};
