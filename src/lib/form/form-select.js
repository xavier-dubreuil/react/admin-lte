import React from 'react';
import PropTypes from 'prop-types';
import {Select} from './select';

export const FormSelect = function (props) {
    let classname = 'form-group';
    let classHelp = 'help-block';
    let classLabel = 'control-label';
    if (props.error) {
        classname += ' has-error';
    }
    if (props.horizontal) {
        classLabel += ' col-sm-2';
        classHelp += ' col-sm-offset-2 col-sm-10';
    }

    return (
        <div className={classname}>
            <label htmlFor={props.id} className={classLabel}>{props.label}</label>
            {props.horizontal &&
                <div className="col-sm-10">
                    <Select {...props}>
                        {props.children}
                    </Select>
                </div>
            }
            {!props.horizontal &&
                <Select {...props}>
                    {props.children}
                </Select>
            }
            {props.help &&
            <span className={classHelp}>{props.help}</span>
            }
        </div>
    );
};

FormSelect.defaultProps = {
    horizontal: false,
    multiple: false,
    emptyOption: false,
    error: false,
};

FormSelect.propTypes = {
    horizontal: PropTypes.bool,
    name: PropTypes.string.isRequired,
    extraClass: PropTypes.string,
    handleChange: PropTypes.func,
    disabled: PropTypes.bool,
    label: PropTypes.any,
    value: PropTypes.any,
    multiple: PropTypes.bool,
    emptyOption: PropTypes.bool,
    options: PropTypes.any.isRequired,
    error: PropTypes.bool,
    help: PropTypes.any,
};
