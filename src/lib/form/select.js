import React from 'react';
import PropTypes from 'prop-types';

const getValue = (option, valueField, valueCallback) => {
    if (valueCallback) {
        return valueCallback(option);
    } else if (typeof(option) === 'object') {
        if (option.hasOwnProperty(valueField)) {
            return option[valueField];
        }
        return false;
    }
    return option;
};

const getLabel = (option, labelField, labelCallback) => {
    if (labelCallback) {
        return labelCallback(option);
    } else if (typeof(option) === 'object') {
        if (option.hasOwnProperty(labelField)) {
            return option[labelField];
        }
        return false;
    }
    return option;
};

export const Select = function (props) {
    let attributes = {
        name: props.name,
    };

    if (props.id) {
        attributes['id'] = props.id;
    }
    if (props.disabled) {
        attributes['disabled'] = 'disabled';
    }
    if (props.multiple) {
        attributes['multiple'] = 'multiple';
        attributes['value'] = props.value.map((value) => getValue(value, props.valueField, props.valueCallback));
    } else {
        attributes['value'] = getValue(props.value, props.valueField, props.valueCallback);
    }
    if (props.placeholder) {
        attributes['placeholder'] = props.placeholder;
    }

    return (
        <select className={'form-control ' + props.extraClass}
            onChange={props.handleChange} {...attributes}>
            {props.emptyOption &&
                <option/>
            }
            {props.options.map((option) => {
                const value = getValue(option, props.valueField, props.valueCallback);
                const label = getLabel(option, props.labelField, props.labelCallback);
                let attributes = {};
                if (typeof(option) === 'object') {
                    attributes['data-value'] = JSON.stringify(option);
                }
                if (value && label) {
                    return (
                        <option key={value} value={value} {...attributes}>{label}</option>
                    );
                }
                return null;
            })}
            {props.children}
        </select>
    );
};

Select.defaultProps = {
    extraClass: '',
    disabled: false,
    multiple: false,
    options: [],
    emptyOption: true,
    valueId: 'id',
};

Select.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string.isRequired,
    extraClass: PropTypes.string,
    handleChange: PropTypes.func,
    disabled: PropTypes.bool,
    placeholder: PropTypes.string,
    value: PropTypes.any,
    options: PropTypes.array,
    emptyOption: PropTypes.bool,
    valueField: PropTypes.string,
    labelField: PropTypes.string,
    valueCallback: PropTypes.func,
    labelCallback: PropTypes.func,
    multiple: PropTypes.bool,
};
