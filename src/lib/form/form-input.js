import React from 'react';
import PropTypes from 'prop-types';
import {Input} from './input';

export const FormInput = function (props) {
    let classname = 'form-group ';
    let classHelp = 'help-block';
    let classLabel = 'control-label';
    if (props.error) {
        classname += ' has-error';
    }
    if (props.horizontal) {
        classLabel += ' col-sm-2';
        classHelp += ' col-sm-offset-2 col-sm-10';
    }

    return (
        <div className={classname}>
            <label htmlFor={props.id} className={classLabel}>{props.label}</label>
            {props.horizontal &&
            <div className="col-sm-10">
                <Input {...props} />
            </div>
            }
            {!props.horizontal &&
            <Input {...props} />
            }
            {props.help &&
            <span className={classHelp}>{props.help}</span>
            }
        </div>
    );
};

FormInput.defaultProps = {
    horizontal: false,
    error: false,
};

FormInput.propTypes = {
    horizontal: PropTypes.bool,
    type: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    extraClass: PropTypes.string,
    handleChange: PropTypes.func,
    disabled: PropTypes.bool,
    label: PropTypes.any,
    value: PropTypes.any,
    error: PropTypes.bool,
    help: PropTypes.any,
    size: PropTypes.string,
};
