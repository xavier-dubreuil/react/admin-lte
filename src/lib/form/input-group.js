import React from 'react';
import PropTypes from 'prop-types';

export const InputGroup = function(props) {
    let className = props.className.split(' ');
    className.push('input-group');
    if (props.margin) {
        className.push('margin');
    }

    return (
        <div className={className.join(' ')}>
            {props.children}
        </div>
    );
};

InputGroup.defaultProps = {
    className: '',
};

InputGroup.propTypes = {
    margin: PropTypes.bool,
};
