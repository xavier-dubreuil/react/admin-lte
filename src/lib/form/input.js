import React from 'react';
import PropTypes from 'prop-types';

export const Input = function(props) {
    let attributes = {
        name: props.name,
        type: props.type,
        value: props.value,
        className: props.extraClass,
    };

    if (props.type === 'checkbox') {
        attributes['checked'] = props.value ? 'checked' : '';
    } else {
        attributes['className'] = ' form-control';
    }
    if (props.id) {
        attributes['id'] = props.id;
    }
    if (props.disabled) {
        attributes['disabled'] = 'disabled';
    }
    if (props.placeholder) {
        attributes['placeholder'] = props.placeholder;
    }
    if (props.size) {
        attributes['className'] += ' input-'+props.size;
    }

    return (
        <input onChange={props.handleChange} {...attributes} />
    );
};

Input.defaultProps = {
    type: 'text',
    extraClass: '',
    disabled: false,
    value: '',
};

Input.propTypes = {
    id: PropTypes.string,
    type: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    extraClass: PropTypes.string,
    handleChange: PropTypes.func,
    disabled: PropTypes.bool,
    placeholder: PropTypes.string,
    value: PropTypes.any,
    size: PropTypes.string,
};
