import React from 'react';
import PropTypes from 'prop-types';

export const Form = function(props) {

    let classes = props.extraClasses.split(' ');
    if (props.horizontal) {
        classes.push('form-horizontal');
    }

    return (
        <div className={classes.join(' ')} role="form">
            {props.children}
        </div>
    );
};

Form.defaultProps = {
    extraClasses: '',
    horizontal: false
};

Form.propTypes = {
    extraClasses: PropTypes.string,
    horizontal: PropTypes.bool,
};
