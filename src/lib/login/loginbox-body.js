import React from 'react';
import PropTypes from 'prop-types';

export const LoginBoxBody = function(props) {

    let classes = props.extraClasses.split(' ');
    classes.push('login-box-body');

    return (
        <div className={classes.join(' ')}>
            {props.children}
        </div>
    );
};

LoginBoxBody.defaultProps = {
    extraClasses: '',
};

LoginBoxBody.propTypes = {
    extraClasses: PropTypes.string,
};
