import React from 'react';
import PropTypes from 'prop-types';

export const LoginBox = function(props) {

    let classes = props.extraClasses.split(' ');
    classes.push('login-box');

    return (
        <div className={classes.join(' ')}>
            {props.children}
        </div>
    );
};

LoginBox.defaultProps = {
    extraClasses: '',
};

LoginBox.propTypes = {
    extraClasses: PropTypes.string,
};
