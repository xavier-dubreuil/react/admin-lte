import React from 'react';
import PropTypes from 'prop-types';

export const LoginBoxMessage = function(props) {

    let classes = props.extraClasses.split(' ');
    classes.push('login-box-msg');

    return (
        <p className={classes.join(' ')}>
            {props.children}
        </p>
    );
};

LoginBoxMessage.defaultProps = {
    extraClasses: '',
};

LoginBoxMessage.propTypes = {
    extraClasses: PropTypes.string,
};
