import React from 'react';
import PropTypes from 'prop-types';

export const LoginBoxHeader = function(props) {

    let classes = props.extraClasses.split(' ');
    classes.push('login-logo');

    return (
        <div className={classes.join(' ')}>
            {props.children}
        </div>
    );
};

LoginBoxHeader.defaultProps = {
    extraClasses: '',
};

LoginBoxHeader.propTypes = {
    extraClasses: PropTypes.string,
};
