import React from 'react';
import PropTypes from 'prop-types';
import {TabPane} from './tab-pane';

export class Tabs extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            current: '',
        };
    }

    changeCurrent(tab) {
        this.setState({current: tab});
    }

    render () {
        let classes = this.props.extraClasses.split(' ');
        classes.push('nav-tabs-custom');

        let children = [];
        if (Array.isArray(this.props.children)) {
            children = this.props.children;
        } else if (this.props.children !== undefined) {
            children = [this.props.children];
        }

        const current = this.state.current !== '' ? this.state.current : children[0].props.title;

        return (
            <div className={classes.join(' ')}>
                <ul className="nav nav-tabs">
                    {children.map((child) => (
                        <li key={child.props.title} className={current === child.props.title ? 'active' : ''}>
                            <a onClick={this.changeCurrent.bind(this, child.props.title)}>
                                {child.props.title}
                            </a>
                        </li>
                    ))}
                </ul>
                <div className="tab-content">
                    {children.map((child) => {
                        if (current === child.props.title) {
                            return (
                                <TabPane key={child.title} {...child.props} active={true}/>
                            );
                        }
                        return '';
                    })}
                </div>
            </div>
        );
    }
}

Tabs.defaultProps = {
    extraClasses: '',
};

Tabs.propTypes = {
    extraClasses: PropTypes.string,
};