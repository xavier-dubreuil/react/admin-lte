import React from 'react';
import PropTypes from 'prop-types';

export class TabPane extends React.Component {
    render () {
        let classes = this.props.extraClasses.split(' ');
        classes.push('tab-pane');
        if (this.props.active) {
            classes.push('active');
        }

        return (
            <div className={classes.join(' ')}>
                {this.props.children}
            </div>
        );
    }
}

TabPane.defaultProps = {
    extraClasses: '',
    active: false,
};

TabPane.propTypes = {
    extraClasses: PropTypes.string,
    title: PropTypes.any.isRequired,
    active: PropTypes.bool,
};