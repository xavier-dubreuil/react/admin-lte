import React from 'react';
import PropTypes from 'prop-types';

export const ContentHeaderBreadcrumbItem = function(props) {
    return (
        <li>
            {props.icon && props.icon}
            {props.title}
        </li>
    );
};

ContentHeaderBreadcrumbItem.propTypes = {
    title: PropTypes.string.isRequired,
    icon: PropTypes.object,
};