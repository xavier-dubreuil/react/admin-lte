import React from 'react';

export const ContentHeaderBreadcrumb = function(props) {
    return (
        <ol className="breadcrumb">
            {props.children}
        </ol>
    );
};
