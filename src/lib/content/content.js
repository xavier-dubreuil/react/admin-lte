import React from 'react';

export const Content = function(props) {
    return (
        <section className="content">
            {props.children}
        </section>
    );
};
