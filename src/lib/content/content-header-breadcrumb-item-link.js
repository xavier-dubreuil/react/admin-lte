import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export const ContentHeaderBreadcrumbItemLink = function(props) {
    return (
        <li>
            <Link to={props.to}>
                {props.icon && props.icon}
                {props.title}
            </Link>
        </li>
    );
};

ContentHeaderBreadcrumbItemLink.propTypes = {
    title: PropTypes.string.isRequired,
    to: PropTypes.string,
    icon: PropTypes.object,
};