import React from 'react';
import PropTypes from 'prop-types';

export const ContentColumn = function(props) {

    let classes = [];
    if (props.lgSize) {
        classes.push('col-lg-' + props.lgSize);
    }
    if (props.mdSize) {
        classes.push('col-md-' + props.mdSize);
    }
    if (props.smSize) {
        classes.push('col-sm-' + props.smSize);
    }
    if (props.xsSize) {
        classes.push('col-xs-' + props.xsSize);
    }
    if (props.lgOffset) {
        classes.push('offset-lg-' + props.lgOffset);
    }
    if (props.mdOffset) {
        classes.push('offset-md-' + props.mdOffset);
    }
    if (props.smOffset) {
        classes.push('offset-sm-' + props.smOffset);
    }
    if (props.xsOffset) {
        classes.push('offset-xs-' + props.xsOffset);
    }
    if (props.classeName) {
        classes.push(props.classeName);
    }

    return (
        <div className={classes.join(' ')}>
            {props.children}
        </div>
    );
};

ContentColumn.propTypes = {
    lgSize: PropTypes.number,
    mdSize: PropTypes.number,
    smSize: PropTypes.number,
    xsSize: PropTypes.number,
    lgOffset: PropTypes.number,
    mdOffset: PropTypes.number,
    smOffset: PropTypes.number,
    xsOffset: PropTypes.number,
    classeName: PropTypes.string,
};