import React from 'react';
import PropTypes from 'prop-types';

export const ContentHeader = function(props) {
    return (
        <section className="content-header">
            <h1>
                {props.title}
                {props.subTitle &&
                    <small>{props.subTitle}</small>
                }
            </h1>
            {props.breadcrumb &&
            <ol className="breadcrumb">
                {props.breadcrumb}
            </ol>
            }
        </section>
    );
};

ContentHeader.propTypes = {
    title: PropTypes.string.isRequired,
    subTitle: PropTypes.string,
    breadcrumb: PropTypes.object,
};