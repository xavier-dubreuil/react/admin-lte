import React from 'react';

export const ContentPageHeader = function(props) {
    return (
        <h2 className="page-header">
            {props.children}
        </h2>
    );
};
