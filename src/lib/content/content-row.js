import React from 'react';

export const ContentRow = function(props) {
    return (
        <div className="row">
            {props.children}
        </div>
    );
};
