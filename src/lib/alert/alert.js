import React from 'react';
import PropTypes from 'prop-types';

export class Alert extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dismiss: false,
        };
    }
    dismiss() {
        this.setState({
            dismiss: true,
        });
    }
    render() {

        let classes = ['alert', 'alert-'+this.props.skin];
        if (this.props.className !== '') {
            classes.push(this.props.className);
        }

        if (!this.state.dismiss) {
            return (
                <div className={classes.join(' ')}>
                    <button type="button" className="close" onClick={this.dismiss.bind(this)}>×</button>
                    {this.props.children}
                </div>
            );
        }
        return (
            <div/>
        );
    }
}

Alert.api = {
    skin: {
        type: 'String',
        desc: 'Skin of the alert',
        isRequired: true,
    },
    className: {
        type: 'String',
        default: '',
        desc: 'Extra CSS classes',
        isRequired: false,
    },
};

Alert.defaultProps = {
    className: Alert.api.className.default,
};

Alert.propTypes = {
    skin: PropTypes.string.isRequired,
    className: PropTypes.string,
};
