import React from 'react';
import PropTypes from 'prop-types';

export class Icon extends React.Component {
    render() {
        const {
            border,
            className,
            fixedWidth,
            flip,
            inverse,
            name,
            pulse,
            rotate,
            size,
            spin,
            stack,
            tag = 'span',
            ariaLabel,
            type,
            ...props
        } = this.props;

        const classNames = [];

        if (type === 'fa') {
            classNames.push('fa');
            classNames.push('fa-' + name);
            size && classNames.push('fa-' + size);
            spin && classNames.push('fa-spin');
            pulse && classNames.push('fa-pulse');
            border && classNames.push('fa-border');
            fixedWidth && classNames.push('fa-fw');
            inverse && classNames.push('fa-inverse');
            flip && classNames.push('fa-flip-' + flip);
            rotate && classNames.push('fa-rotate-' + rotate);
            stack && classNames.push('fa-stack-' + stack);
        } else if (type === 'ion') {
            classNames.push('ion');
            classNames.push('ion-' + name);
        } else if (type === 'glyph') {
            classNames.push('glyphicon');
            classNames.push('glyphicon-' + name);
        }

        // Add any custom class names at the end.
        className && classNames.push(className);
        return React.createElement(
            tag,
            {...props, 'aria-hidden': true, className: classNames.join(' ')},
            ariaLabel
                ? React.createElement('span', {ariaLabel})
                : null
        );
    }
}

Icon.propTypes = {
    type: PropTypes.string.isRequired,
    ariaLabel: PropTypes.string,
    border: PropTypes.bool,
    className: PropTypes.string,
    fixedWidth: PropTypes.bool,
    flip: PropTypes.oneOf([ 'horizontal', 'vertical' ]),
    inverse: PropTypes.bool,
    name: PropTypes.string.isRequired,
    pulse: PropTypes.bool,
    rotate: PropTypes.oneOf([ 90, 180, 270 ]),
    size: PropTypes.oneOf([ 'lg', '2x', '3x', '4x', '5x' ]),
    spin: PropTypes.bool,
    stack: PropTypes.oneOf([ '1x', '2x' ]),
    tag: PropTypes.string,
};
