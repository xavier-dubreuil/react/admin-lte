import React from 'react';
import FontAwesome from 'react-fontawesome';

export const BoxLoading = function () {
    return (
        <div className="overlay">
            <FontAwesome name="refresh" spin={true}/>
        </div>
    );
};
