import React from 'react';
import PropTypes from 'prop-types';

export const BoxFooter = function(props) {

    let classes = props.classes ? props.classes : [];
    classes.push('box-footer');

    return (
        <div className={classes.join(' ')}>
            {props.children}
        </div>
    );
};

BoxFooter.propTypes = {
    classes: PropTypes.array,
};