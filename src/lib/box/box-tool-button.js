import React from 'react';
import PropTypes from 'prop-types';

export const BoxToolButton = function(props) {


    let classes = ['btn', 'btn btn-box-tool'];
    if (props.className !== '') {
        classes.push(props.className);
    }

    return (
        <button className={classes.join(' ')} onClick={props.onClick}>
            {props.children}
        </button>
    );
};

BoxToolButton.defaultProps = {
    className: '',
};

BoxToolButton.PropTypes = {
    onClick: PropTypes.func,
};


