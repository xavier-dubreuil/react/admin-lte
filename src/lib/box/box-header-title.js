import React from 'react';

export const BoxHeaderTitle = function(props) {
    return (
        <h3 className="box-title">{props.children}</h3>
    );
};
