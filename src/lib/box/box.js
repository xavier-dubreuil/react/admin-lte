import React from 'react';
import PropTypes from 'prop-types';
import {BoxHeader} from './box-header';
import {BoxHeaderTitle} from './box-header-title';
import {BoxHeaderTools} from './box-header-tools';
import {BoxFooter} from './box-footer';
import {BoxLoading} from './box-loading';
import {BoxBody} from './box-body';
import {BoxToolButton} from './box-tool-button';
import {Icon} from '../icon';

export class Box extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            collapsed: props.collapsed,
            closed: false,
        };
    }

    toggleCollapse() {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    closeBox() {
        this.setState({
            closed: true,
        });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            collapsed: nextProps.collapsed,
        });
    }

    render() {

        if (this.state.closed) {
            return (<div/>);
        }
        let classes = ['box'];
        if (this.props.solid) {
            classes.push('box-solid');
        }
        if (this.props.skin) {
            classes.push('box-' + this.props.skin);
        }
        if (this.props.className !== '') {
            classes.push(this.props.className);
        }

        let showContent = false;
        if (!this.state.collapsed && this.props.collapsable) {
            showContent = true;
        } else if (!this.props.collapsable) {
            showContent = true;
        }
        if (this.props.header || this.props.title || this.props.titleTools || this.props.footer) {
            return (
                <div className={classes.join(' ')}>
                    {this.props.header && <BoxHeader>{this.props.header}</BoxHeader>}
                    {(this.props.title || this.props.titleTools) &&
                    <BoxHeader>
                        <BoxHeaderTitle>{this.props.title}</BoxHeaderTitle>
                        <BoxHeaderTools>
                            {this.props.titleTools}
                            {this.props.collapsable && !this.props.collapsed &&
                            <BoxToolButton onClick={this.toggleCollapse.bind(this)}>
                                <Icon tag="i" type="fa" name="minus"/>
                            </BoxToolButton>
                            }
                            {this.props.collapsable && this.props.collapsed &&
                            <BoxToolButton onClick={this.toggleCollapse.bind(this)}>
                                <Icon tag="i" type="fa" name="plus"/>
                            </BoxToolButton>
                            }
                            {this.props.closable &&
                            <BoxToolButton onClick={this.closeBox.bind(this)}>
                                <Icon tag="i" type="fa" name="times"/>
                            </BoxToolButton>
                            }
                        </BoxHeaderTools>
                    </BoxHeader>
                    }
                    {showContent &&
                    <BoxBody textCenter={this.props.textCenter}>{this.props.children}</BoxBody>
                    }
                    {showContent && this.props.footer &&
                    <BoxFooter>{this.props.footer}</BoxFooter>}
                    {this.props.loading && <BoxLoading/>}
                </div>
            );
        }
        return (
            <div className={classes.join(' ')}>
                {this.props.children}
                {this.props.loading && <BoxLoading/>}
            </div>
        );
    }
}

Box.defaultProps = {
    loading: false,
    solid: false,
    className: '',
    collapsable: false,
    collapsed: false,
    closable: false,
};

Box.propTypes = {
    className: PropTypes.string,
    skin: PropTypes.string,
    header: PropTypes.any,
    title: PropTypes.any,
    titleTools: PropTypes.any,
    footer: PropTypes.any,
    loading: PropTypes.bool,
    solid: PropTypes.bool,
    textCenter: PropTypes.bool,
    collapsable: PropTypes.bool,
    collapsed: PropTypes.bool,
    closable: PropTypes.bool,
};