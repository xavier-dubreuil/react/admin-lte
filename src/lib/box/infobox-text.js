import React from 'react';
import PropTypes from 'prop-types';

export const InfoBoxText = function(props) {

    let classes = ['info-box-text', props.extraClasses];

    return (
        <div className={classes.join(' ')}>
            {props.children}
        </div>
    );
};

InfoBoxText.defaultProps = {
    extraClasses: '',
};

InfoBoxText.propTypes = {
    extraClasses: PropTypes.string,
};