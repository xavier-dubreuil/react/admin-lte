import React from 'react';
import PropTypes from 'prop-types';

export const InfoBox = function(props) {

    let classes = ['info-box'];
    if (props.color) {
        classes.push('bg-' + props.color);
    }

    let iconClasses = ['info-box-icon'];
    if (props.iconColor) {
        iconClasses.push('bg-' + props.iconColor);
    }

    return (
        <div className={classes.join(' ')}>
            <span className={iconClasses.join(' ')}>
                {props.icon}
            </span>

            <div className="info-box-content">
                {props.children}
            </div>
        </div>
    );
};

InfoBox.defaultProps = {
    isLoading: false,
};

InfoBox.propTypes = {
    color: PropTypes.string,
    iconColor: PropTypes.string,
    icon: PropTypes.any,
    isLoading: PropTypes.bool,
};