import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export const SmallBox = function(props) {

    let classes = ['small-box'];
    if (props.color) {
        classes.push('bg-' + props.color);
    }

    return (
        <div className={classes.join(' ')}>
            <div className="inner">
                {props.children}
            </div>
            <div className="icon">
                {props.icon}
            </div>
            {props.footer && props.to &&
                <Link to={props.to} className="small-box-footer">
                    {props.footer}
                </Link>
            }
        </div>
    );
};

SmallBox.propTypes = {
    color: PropTypes.string,
    icon: PropTypes.object,
    footer: PropTypes.object,
    to: PropTypes.string,
};