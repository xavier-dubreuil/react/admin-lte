import React from 'react';

export const BoxHeaderTools = function(props) {
    return (
        <div className="box-tools">
            {props.children}
        </div>
    );
};
