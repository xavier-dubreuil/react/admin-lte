import React from 'react';
import PropTypes from 'prop-types';

export const BoxHeader = function(props) {

    let classes = props.classes ? props.classes : [];
    classes.push('box-header');
    if (props.bordered) {
        classes.push('with-border');
    }

    return (
        <div className={classes.join(' ')}>
            {props.children}
        </div>
    );
};

BoxHeader.propTypes = {
    bordered: PropTypes.bool,
    classes: PropTypes.array,
};