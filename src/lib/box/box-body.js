import React from 'react';
import PropTypes from 'prop-types';

export const BoxBody = function(props) {

    let classes = props.extraClasses.split(' ');
    classes.push('box-body');
    if (props.textCenter) {
        classes.push('text-center');
    }

    return (
        <div className={classes.join(' ')}>
            {props.children}
        </div>
    );
};

BoxBody.defaultProps = {
    extraClasses: '',
    textCenter: false,
};

BoxBody.propTypes = {
    extraClasses: PropTypes.string,
    textCenter: PropTypes.bool,
};