import React from 'react';
import PropTypes from 'prop-types';

export const InfoBoxNumber = function(props) {

    let classes = ['info-box-number', props.extraClasses];

    return (
        <div className={classes.join(' ')}>
            {props.children}
        </div>
    );
};

InfoBoxNumber.defaultProps = {
    extraClasses: '',
};

InfoBoxNumber.propTypes = {
    extraClasses: PropTypes.string,
};