import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

const TopNavigation = function (props) {
    return (
        <header className="main-header">
            <nav className="navbar navbar-static-top">
                <div className="container">
                    <div className="navbar-header">
                        <Link to="/" className="navbar-brand">{props.title}</Link>
                    </div>
                    <div className="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <props.navigation/>
                    </div>
                    <div className="navbar-custom-menu">
                        <props.navbar/>
                    </div>
                </div>
            </nav>
        </header>
    );
};

const DefaultNavigation = function (props) {
    return (
        <header className="main-header">
            <Link to="/" className="logo">
                <span className="logo-mini">{props.logoMini}</span>
                <span className="logo-lg">{props.logoLarge}</span>
            </Link>
            <nav className="navbar navbar-static-top">
                {props.sidebar && props.collapsableSidebar &&
                <span className="sidebar-toggle" data-toggle="push-menu" role="button"
                    onClick={props.toggleSidebar}>
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar" />
                    <span className="icon-bar" />
                    <span className="icon-bar" />
                </span>
                }
                <props.navbar toggleControlbar={props.toggleControlbar} />
            </nav>
        </header>
    );
};

export class AdminLayout extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            collapsedSidebar: props.collapsedSidebar,
            showControlbar: props.showControlbar,
        };
    }

    toggleSidebar() {
        this.setState({
            collapsedSidebar: !this.state.collapsedSidebar,
        });
    }

    toggleControlbar() {
        this.setState({
            showControlbar: !this.state.showControlbar,
        });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            collapsedSidebar: nextProps.collapsedSidebar,
            showControlbar: nextProps.showControlbar,
        });
    }

    render() {

        let classes = [];
        classes.push('skin-' + this.props.skin);
        if (this.props.layout === 'fixed') {
            classes.push('fixed');
        } else if (this.props.layout === 'boxed') {
            classes.push('layout-boxed');
        } else if (this.props.layout === 'top-navigation') {
            classes.push('layout-top-nav');
        }
        if (this.state.collapsedSidebar) {
            classes.push('sidebar-collapse');
        }
        if (this.props.sideControlbar) {
            classes.push('control-sidebar-open');
        }
        if (this.props.miniSidebar) {
            classes.push('sidebar-mini');
        }

        let controlbarClasses = ['control-sidebar'];
        controlbarClasses.push(this.props.lightControlbar ? 'control-sidebar-light' : 'control-sidebar-dark');
        if (this.state.showControlbar) {
            controlbarClasses.push('control-sidebar-open');
        }

        const contentHeight = this.props.footer ? 'calc(100vh - 80px)' : 'calc(100vh - 50px)';
        let contentStyles = {
            minHeight: contentHeight,
        };
        if (!this.props.sidebar) {
            contentStyles['marginLeft'] = 0;
        }

        return (
            <div className={classes.join(' ')}>
                <div className="wrapper" style={{height: 'auto', minHeight: '100vh'}}>
                    {this.props.layout === 'top-navigation' &&
                    <TopNavigation title={this.props.logoLarge} navigation={this.props.navigation}
                        navbar={this.props.navbar} />
                    }
                    {this.props.layout !== 'top-navigation' &&
                    <DefaultNavigation logoMini={this.props.logoMini} logoLarge={this.props.logoLarge}
                        navbar={this.props.navbar} toggleSidebar={this.toggleSidebar.bind(this)}
                        toggleControlbar={this.toggleControlbar.bind(this)} sidebar={this.props.sidebar}
                        collapsableSidebar={this.props.collapsableSidebar} />
                    }
                    {this.props.layout !== 'top-navigation' && this.props.sidebar &&
                    <aside className="main-sidebar">
                        <section className="sidebar" style={{height: 'auto'}}>
                            {this.props.sidebar}
                        </section>
                    </aside>
                    }
                    <div className="content-wrapper" style={contentStyles}>
                        {this.props.layout === 'top-navigation' &&
                        <div className="container">
                            {this.props.children}
                        </div>
                        }
                        {this.props.layout !== 'top-navigation' && this.props.children}
                    </div>
                    {this.props.footer &&
                    <footer className="main-footer">
                        {this.props.footer}
                    </footer>
                    }
                    {this.props.controlbar &&
                    <aside className={controlbarClasses.join(' ')} style={{minHeight: contentHeight}}>
                        {this.props.controlbar}
                    </aside>
                    }
                </div>
            </div>
        );
    }
}

AdminLayout.defaultProps = {
    layout: 'default',
    collapsableSidebar: true,
    collapsedSidebar: false,
    miniSidebar: true,
    showControlbar: false,
    sideControlbar: false,
    lightControlbar: false,
};

AdminLayout.propTypes = {
    skin: PropTypes.string.isRequired,
    logoMini: PropTypes.string.isRequired,
    logoLarge: PropTypes.any.isRequired,
    layout: PropTypes.string,
    topNavigation: PropTypes.any,
    navbar: PropTypes.any,
    sidebar: PropTypes.any,
    footer: PropTypes.any,
    controlbar: PropTypes.any,
    collapsableSidebar: PropTypes.bool,
    collapsedSidebar: PropTypes.bool,
    miniSidebar: PropTypes.bool,
    showControlbar: PropTypes.bool,
    sideControlbar: PropTypes.bool,
    lightControlbar: PropTypes.bool,
};
