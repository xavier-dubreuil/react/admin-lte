import React from 'react';
import PropTypes from 'prop-types';

export const LoginLayout = function (props) {

    let classes = props.extraClasses.split(' ');
    classes.push('hold-transition');
    classes.push('login-page');

    return (
        <div className={classes.join(' ')}>
            {props.children}
        </div>
    );
};

LoginLayout.defaultProps = {
    extraClasses: '',
};

LoginLayout.propTypes = {
    extraClasses: PropTypes.string,
};
