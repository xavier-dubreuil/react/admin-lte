import React from 'react';
import PropTypes from 'prop-types';

export const Table = function(props) {

    let classes = props.classes ? props.classes : [];
    classes.push('table');
    if (props.bordered) {
        classes.push('table-bordered');
    }

    return (
        <table className={classes.join(' ')}>
            {props.children}
        </table>
    );
};

Table.propTypes = {
    bordered: PropTypes.bool,
    classes: PropTypes.array,
};