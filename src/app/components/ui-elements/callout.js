import React from 'react';
import {DemoGenerator, generateAttributes} from '../demo/demo';
import {
    Callout,
    FormControl,
    Input,
    Select,
} from '../../../lib/index';

const imports = [
    {
        from: 'react-xd-admin-lte',
        imports: ['ContentRow', 'ContentColumn', 'InfoBox', 'InfoBoxText', 'InfoBoxNumber', 'ProgressBar', 'ProgressDescription']
    },
];

const state = {
    skin: 'danger',
    title: 'I am a callout!',
    text: 'Callout message',
};

const paramsOptions = function (props) {
    return (
        <div>
            <FormControl label="Alert Skin" horizontal={false}>
                <Select name="skin" handleChange={props.handleChange} value={props.state.skin} emptyOption={false}>
                    <option value="danger">Danger</option>
                    <option value="info">Info</option>
                    <option value="warning">Warning</option>
                    <option value="success">Success</option>
                </Select>
            </FormControl>
            <FormControl label="Title" horizontal={false}>
                <Input type="text" name="title" handleChange={props.handleChange} value={props.state.title} />
            </FormControl>
            <FormControl label="Text" horizontal={false}>
                <Input type="text" name="text" handleChange={props.handleChange} value={props.state.text} />
            </FormControl>
        </div>
    );
};

const myComponent = function (props) {
    const demoAttributes = generateAttributes(props, ['skin']);

    return (
        <Callout {...demoAttributes}>
            <h4>{props.title}</h4>
            <p>{props.text}</p>
        </Callout>
    );
};

export const CalloutGenerator = function () {
    return (
        <DemoGenerator state={state} component={myComponent} imports={imports} api={[Callout]} params={paramsOptions} />
    );
};