import React from 'react';
import {DemoGenerator, generateAttributes} from '../demo/demo';
import {
    Box,
    ProgressBar,
    ProgressDescription,
    FormControl,
    Input,
    Select,
} from '../../../lib/index';

const imports = [
    {
        from: 'react-xd-admin-lte',
        imports: ['ContentRow', 'ContentColumn', 'InfoBox', 'InfoBoxText', 'InfoBoxNumber', 'ProgressBar', 'ProgressDescription']
    },
];

const state = {
    skin: 'danger',
    percent: 50,
    vertical: false,
    striped: false,
    active: false,
    size: '',
    description: 'Progress bar description'
};

const paramsOptions = function (props) {
    return (
        <div>
            <FormControl label="Alert Skin" horizontal={false}>
                <Select name="skin" handleChange={props.handleChange}
                    value={props.state.skin} emptyOption={false}>
                    <option value="danger">Danger</option>
                    <option value="info">Info</option>
                    <option value="warning">Warning</option>
                    <option value="success">Success</option>
                </Select>
            </FormControl>
            <FormControl label="Size" horizontal={false}>
                <Select name="size" handleChange={props.handleChange}
                    value={props.state.size} emptyOption={false}>
                    <option value="">Default</option>
                    <option value="sm">Medium</option>
                    <option value="xs">Small</option>
                    <option value="xxs">Extra small</option>
                </Select>
            </FormControl>
            <FormControl label="Percent" horizontal={false}>
                <Input type="number" name="percent" handleChange={props.handleChange}
                    value={props.state.percent} />
            </FormControl>
            <FormControl label="Vertical" horizontal={false}>
                <Input type="checkbox" name="vertical" handleChange={props.handleChange}
                    value={props.state.vertical} />
            </FormControl>
            <FormControl label="Striped" horizontal={false}>
                <Input type="checkbox" name="striped" handleChange={props.handleChange}
                    value={props.state.striped} />
            </FormControl>
            <FormControl label="Active" horizontal={false}>
                <Input type="checkbox" name="active" handleChange={props.handleChange}
                    value={props.state.active} />
            </FormControl>
            <FormControl label="Description" horizontal={false}>
                <Input type="text" name="description" handleChange={props.handleChange}
                    value={props.state.description} />
            </FormControl>
        </div>
    );
};

const myComponent = function (props) {

    const demoAttributes = generateAttributes(props, ['skin', 'percent', 'striped', 'active', 'vertical', 'size']);

    return (
        <Box title="Progress Bar">
            <ProgressBar {...demoAttributes} />
            {props.description &&
            <ProgressDescription>{props.description}</ProgressDescription>
            }
        </Box>
    );
};

export const ProgressBarGenerator = function () {
    return (
        <DemoGenerator state={state} component={myComponent} imports={imports} api={[ProgressBar, ProgressDescription]}
            params={paramsOptions} additionalComponents={[]} />
    );
};
