import React from 'react';
import {DemoGenerator, generateAttributes} from '../demo/demo';
import {
    Button,
    Skins,
    FormControl,
    Select,
    Input,
    Box,
} from '../../../lib/index';

const imports = [
    {
        from: 'react-xd-admin-lte',
        imports: ['ContentRow', 'ContentColumn', 'Box', 'Button'],
    },
];

const state = {
    skin: 'default',
    text: 'Button',
};

const paramsOptions = function (props) {
    return (
        <div>
            <FormControl label="Box Skin" horizontal={false}>
                <Select name="skin" handleChange={props.handleChange}
                    value={props.state.skin} emptyOption={false}>
                    {Skins.map((skin) => (
                        <option key={skin} value={skin}>{skin}</option>
                    ))}
                </Select>
            </FormControl>
            <FormControl label="Text" horizontal={false}>
                <Input type="text" name="text" handleChange={props.handleChange}
                    value={props.state.text} />
            </FormControl>
        </div>
    );
};

const Demo = function (props) {
    const demoAttributes = generateAttributes(props, ['size', 'skin']);

    return (
        <Box title="Button" textCenter={true}>
            <Button {...demoAttributes}>
                {props.text}
            </Button>
        </Box>
    );
};

export const ButtonGenerator = function () {
    return (
        <DemoGenerator state={state} component={Demo} imports={imports} api={[Button]} params={paramsOptions} />
    );
};
