import React from 'react';
import {DemoGenerator, generateAttributes, generateIcon} from '../demo/demo';
import {
    Alert,
    FormControl,
    Input,
    Select,
} from '../../../lib/index';

const imports = [
    {
        from: 'react-xd-admin-lte',
        imports: ['ContentRow', 'ContentColumn', 'Alert', 'Icon'],
    },
];

const state = {
    skin: 'danger',
    iconType: 'fa',
    iconName: 'ban',
    text: 'Alert message',
};

const paramsOptions = function (props) {
    return (
        <div>
            <FormControl label="Alert Skin" horizontal={false}>
                <Select name="skin" handleChange={props.handleChange} value={props.state.skin} emptyOption={false}>
                    <option value="danger">Danger</option>
                    <option value="info">Info</option>
                    <option value="warning">Warning</option>
                    <option value="success">Success</option>
                </Select>
            </FormControl>
            <FormControl label="Icon Type" horizontal={false}>
                <Select name="iconType" handleChange={props.handleChange} value={props.state.iconType}
                    emptyOption={false}>
                    <option value="">No Icon</option>
                    <option value="fa">Font Awesome</option>
                    <option value="glyph">Glyph Icons</option>
                    <option value="ion">Ion Icons</option>
                </Select>
            </FormControl>
            <FormControl label="Icon Name" horizontal={false}>
                <Input type="text" name="iconName" handleChange={props.handleChange} value={props.state.iconName} />
            </FormControl>
            <FormControl label="Text" horizontal={false}>
                <Input type="text" name="text" handleChange={props.handleChange} value={props.state.text} />
            </FormControl>
        </div>
    );
};

const Demo = function (props) {
    const demoAttributes = generateAttributes(props, ['skin']);
    const icon = generateIcon(props.iconType, props.iconName);

    return (
        <Alert {...demoAttributes}>
            <h4>{icon} Alert!</h4>
            {props.text}
        </Alert>
    );
};

export const AlertGenerator = function () {
    return (
        <DemoGenerator state={state} component={Demo} imports={imports} api={[Alert]} params={paramsOptions} />
    );
};