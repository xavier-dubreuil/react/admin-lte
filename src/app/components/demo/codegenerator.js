import componentToJSXString from './component-to-jsx-string';

export default function (imports, components, props) {
    const params = {maxInlineAttributesLineLength: 400000};
    let source = '';
    source += imports.map((imp) => {
        let line = 'import ';
        if (imp.hasOwnProperty('defaultImport')) {
            line += imp['defaultImport'];
        }
        if (imp.imports) {
            if (imp.hasOwnProperty('defaultImport')) {
                line += ', ';
            }
            line += '{' + imp.imports.join(', ') + '}';
        }
        line += ' from \'' + imp.from + '\';';
        return line;
    }).join('\n');
    source += '\n';
    for (let component in components) {
        if (components.hasOwnProperty(component)) {
            source += '\n';
            source += 'const '+components[component].name+' = function (props) {\n';
            source += '    return (\n';
            const lines = componentToJSXString(components[component](props), params);
            source += lines.split('\n').map((line) => {
                return '         ' + line;
            }).join('\n');
            source += '\n';
            source += '    );\n';
            source += '};\n';
        }
    }
    return source;
}
