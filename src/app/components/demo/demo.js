import React from 'react';
import PropTypes from 'prop-types';
import {PrismCode} from './prismcode';
import generateCode from './codegenerator';
import {
    Box,
    BoxHeaderTitle,
    Content,
    ContentHeader,
    ContentPageHeader,
    ContentRow,
    ContentColumn,
    Table,
    Icon,
} from '../../../lib/index';
import 'prismjs/themes/prism-okaidia.css';

const defaultImports = [
    {
        from: 'react',
        defaultimport: 'React'
    },
];

export const generateAttributes = function (props, keys) {
    let attributes = {};

    for (let key in keys) {
        if (keys.hasOwnProperty(key) && props[keys[key]]) {
            attributes[keys[key]] = props[keys[key]];
        }
    }
    return attributes;
};

export const generateIcon = function (type, name) {
    return (
        <Icon tag="i" type={type} name={name} />
    );
};

// export const LayoutOptions = function (props) {
//     return (
//         <div>
//             <FormControl label="lg Size" horizontal={false}>
//                 <Select name="lgSize" handleChange={props.handleChange}
//                         value={props.state.lgSize} valuetype="number">
//                     {new Array(12).fill(1).map((el, i) => (
//                         <option key={i + 1} value={i + 1}>col-lg-{i + 1}</option>
//                     ))}
//                 </Select>
//             </FormControl>
//             <FormControl label="md Size" horizontal={false}>
//                 <Select name="mdSize" handleChange={props.handleChange}
//                         value={props.state.mdSize} valuetype="number">
//                     {new Array(12).fill(1).map((el, i) => (
//                         <option key={i + 1} value={i + 1}>col-md-{i + 1}</option>
//                     ))}
//                 </Select>
//             </FormControl>
//             <FormControl label="sm Size" horizontal={false}>
//                 <Select name="smSize" handleChange={props.handleChange}
//                         value={props.state.smSize} valuetype="number">
//                     {new Array(12).fill(1).map((el, i) => (
//                         <option key={i + 1} value={i + 1}>col-sm-{i + 1}</option>
//                     ))}
//                 </Select>
//             </FormControl>
//             <FormControl label="xs Size" horizontal={false}>
//                 <Select name="xsSize" handleChange={props.handleChange}
//                         value={props.state.xsSize} valuetype="number">
//                     {new Array(12).fill(1).map((el, i) => (
//                         <option key={i + 1} value={i + 1}>col-xs-{i + 1}</option>
//                     ))}
//                 </Select>
//             </FormControl>
//         </div>
//     );
// };

export class DemoGenerator extends React.Component {
    constructor(props) {
        super(props);
        this.state = props.state;
    }

    handleChange(event) {
        const target = event.target;
        const name = target.name;

        let value = '';
        if (target.type === 'select-one') {
            const option = target.options[target.options.selectedIndex];
            value = option.value;
        } else if (target.type === 'checkbox') {
            value = target.checked;
        } else if (target.type === 'number' || target.getAttribute('data-type') === 'number') {
            value = parseFloat(target.value);
        } else {
            value = target.value;
        }

        this.setState({
            [name]: value
        });
    }

    render() {
        const imports = defaultImports.concat(this.props.imports);
        let components = this.props.additionalComponents || [];
        components.push(this.props.component);
        return (
            <div>
                <ContentHeader title="Box" subTitle="Boxes" />
                <Content>
                    <ContentPageHeader>Demo</ContentPageHeader>
                    <ContentRow>
                        <ContentColumn mdSize={6} xsSize={12}>
                            <this.props.component {...this.state} />
                        </ContentColumn>
                        {this.props.params &&
                        <ContentColumn mdSize={6} xsSize={12}>
                            <Box title="Content Options">
                                <this.props.params handleChange={this.handleChange.bind(this)} state={this.state} />
                            </Box>
                        </ContentColumn>
                        }
                    </ContentRow>
                    <ContentRow>
                        <ContentColumn xsSize={12}>
                            <Box title="Code">
                                <PrismCode language="jsx" component="pre">
                                    {generateCode(imports, components, this.state)}
                                </PrismCode>
                            </Box>
                        </ContentColumn>
                    </ContentRow>
                    {this.props.api.length > 0 &&
                    <div>
                        <ContentPageHeader>API</ContentPageHeader>
                        <ContentRow>
                            {this.props.api.map((api) => {
                                if (api.props && Object.keys(api.props).length > 0) {
                                    return (
                                        <ContentColumn key={api.name} mdSize={6} xsSize={12}>
                                            <Box title={<BoxHeaderTitle>{api.name}</BoxHeaderTitle>}>
                                                <Table>
                                                    <thead>
                                                        <tr>
                                                            <th>Param</th>
                                                            <th>Type</th>
                                                            <th>Required</th>
                                                            <th>Default</th>
                                                            <th>Description</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {Object.keys(api.props).map((prop) => (
                                                            <tr key={prop}>
                                                                <td>{prop}</td>
                                                                <td>{api.props[prop].type}</td>
                                                                <td>{api.props[prop].isRequired ? 'Yes' : 'No'}</td>
                                                                <td>{api.defaultProps && api.defaultProps.hasOwnProperty(prop) && api.defaultProps[prop]}</td>
                                                                <td>{api.props[prop].desc}</td>
                                                            </tr>
                                                        ))}
                                                    </tbody>
                                                </Table>
                                            </Box>
                                        </ContentColumn>
                                    );
                                }
                                return '';
                            })}

                        </ContentRow>
                    </div>
                    }
                </Content>
            </div>
        );
    }
}

DemoGenerator.defaultProps = {
    api: [],
};

DemoGenerator.propTypes = {
    state: PropTypes.object.isRequired,
    component: PropTypes.func.isRequired,
    additionalComponents: PropTypes.array,
    api: PropTypes.array,
    params: PropTypes.func,
    imports: PropTypes.array,
};