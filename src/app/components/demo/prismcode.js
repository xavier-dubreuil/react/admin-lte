import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Prism from 'prismjs';
import 'prismjs/components/prism-jsx';

export class PrismCode extends PureComponent {

    componentDidMount() {
        this._hightlight();
    }

    componentDidUpdate() {
        this._hightlight();
    }

    _hightlight() {
        Prism.highlightElement(this._domNode, this.props.async);
    }

    _handleRefMount(domNode) {
        this._domNode = domNode;
    }

    render() {
        const { language, component: Wrapper, children } = this.props;

        return (
            <Wrapper ref={this._handleRefMount.bind(this)} className={'language-'+language}>
                {children}
            </Wrapper>
        );
    }
}

PrismCode.defaultProps = {
    component: 'code',
    async: false,
};

PrismCode.propTypes = {
    async: PropTypes.bool,
    language: PropTypes.string,
    children: PropTypes.any,
    component: PropTypes.node,
};