import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Switch, Route} from 'react-router-dom';
import {Content} from '../../../lib/content/content';
import {ContentHeader} from '../../../lib/content/content-header';
import {ContentRow} from '../../../lib/content/content-row';
import {ContentColumn} from '../../../lib/content/content-column';
import {changeParameter} from '../../actions/admin-layout-actions';
import {Box} from '../../../lib/box/box';
import {AdminLayout} from '../../../lib/layout/admin';
import generateCode from '../demo/codegenerator';
import {PrismCode} from '../demo/prismcode';
import {Select} from '../../../lib/form/select';
import {FormControl} from '../../../lib/form/form-control';

const imports = [
    {
        from: 'react',
        defaultImport: 'React',
    },
    {
        from: 'react-router-dom',
        imports: ['Switch', 'Router'],
    },
    {
        from: 'react-xd-admin-lte',
        imports: ['AdminLayout'],
    },
];

const IconLayout = function (props) {
    let divattr = {};
    if (props.blacked) {
        divattr = {
            style: {boxShadow: '0 0 2px rgba(0,0,0,0.1)'},
            className: 'clearfix',
        };
    }

    return (
        <li style={{float: 'left', width: '33.33333%', padding: '5px'}}>
            <div data-skin={'skin-' + props.skin} style={{display: 'block', boxShadow: '0 0 3px rgba(0,0,0,0.4)'}}
                className="clearfix full-opacity-hover" onClick={() => {props.setSkin(props.skin);}}>
                <div {...divattr}>
                    <span className={props.class1}
                        style={{
                            display: 'block',
                            width: '20%',
                            float: 'left',
                            height: '28px',
                            background: props.bg1
                        }} />
                    <span className={props.class2}
                        style={{
                            display: 'block',
                            width: '80%',
                            float: 'left',
                            height: '28px',
                            background: props.bg2
                        }} />
                </div>
                <div>
                    <span className={props.class3} style={{
                        display: 'block',
                        width: '20%',
                        float: 'left',
                        height: '80px',
                        background: props.bg3
                    }} />
                    <span className={props.class4} style={{
                        display: 'block',
                        width: '80%',
                        float: 'left',
                        height: '80px',
                        background: props.bg4
                    }} />
                </div>
            </div>
            <p className="text-center no-margin" style={{fontSize: '12px'}}>{props.children}</p>
        </li>
    );
};

IconLayout.propTypes = {
    blacked: PropTypes.bool,
    class1: PropTypes.string,
    class2: PropTypes.string,
    class3: PropTypes.string,
    class4: PropTypes.string,
    bg1: PropTypes.string,
    bg2: PropTypes.string,
    bg3: PropTypes.string,
    bg4: PropTypes.string,
};

const MyComponent = function (props) {
    let attributes = {
        logoMini: 'ALTE',
        logoLarge: 'Admin LTE',
        skin: props.skin,
        navbar: () => (<div />),
    };
    if (props.layout !== 'default') {
        attributes['layout'] = props.layout;
    }
    if (props.sidebar !== 'none') {
        attributes['sidebar'] = <div />;
    }
    if (props.sidebar !== 'none') {
        attributes['controlbar'] = <div />;
    }
    if (props.sidebar === 'collapsed') {
        attributes['collapsedSidebar'] = true;
    }
    if (props.controlbar === 'shown') {
        attributes['showControlbar'] = true;
    }
    if (props.controlbarPosition === 'side') {
        attributes['sideControlbar'] = true;
    }
    if (props.controlbarColor === 'light') {
        attributes['lightControlbar'] = true;
    }

    return (
        <AdminLayout {...attributes}>
            <Switch>
                <Route path="/" component={MyComponent} />
            </Switch>
        </AdminLayout>
    );
};

class AdminLayoutGenerator extends React.Component {

    setSkin(skin) {
        const {dispatch} = this.props;
        dispatch(changeParameter('skin', skin));
    }

    handleChange(event) {

        const {name, value} = this._handleChange(event);

        const {dispatch} = this.props;
        dispatch(changeParameter(name, value));
    }

    _handleChange(event) {
        const target = event.target;
        const name = target.name;

        let value = '';
        if (target.type === 'select-one') {
            const option = target.options[target.options.selectedIndex];
            value = option.value;
        } else if (target.type === 'checkbox') {
            value = target.checked;
        } else if (target.type === 'number' || target.getAttribute('data-type') === 'number') {
            value = parseFloat(target.value);
        } else {
            value = target.value;
        }
        return {name, value};
    }

    render() {
        return (
            <div>
                <ContentHeader title="Admin Layout" />
                <Content>

                    <ContentRow>
                        <ContentColumn mdSize={6} xsSize={12}>
                            <Box skin="primary" title="Skin">
                                <ul className="list-unstyled clearfix">
                                    <IconLayout skin="blue" bg1="#367fa9" class2="bg-light-blue" bg3="#222d32"
                                        bg4="#f4f5f7"
                                        setSkin={this.setSkin.bind(this)}>Bleu</IconLayout>
                                    <IconLayout skin="black" bg1="#fefefe" bg2="#fefefe" bg3="#222" bg4="#f4f5f7"
                                        blacked={true}
                                        setSkin={this.setSkin.bind(this)}>Black</IconLayout>
                                    <IconLayout skin="purple" class1="bg-purple-active" class2="bg-purple" bg3="#222d32"
                                        bg4="#f4f5f7" setSkin={this.setSkin.bind(this)}>Purple</IconLayout>
                                    <IconLayout skin="green" class1="bg-green-active" class2="bg-green" bg3="#222d32"
                                        bg4="#f4f5f7"
                                        setSkin={this.setSkin.bind(this)}>Green</IconLayout>
                                    <IconLayout skin="red" class1="bg-red-active" class2="bg-red" bg3="#222d32"
                                        bg4="#f4f5f7"
                                        setSkin={this.setSkin.bind(this)}>Red</IconLayout>
                                    <IconLayout skin="yellow" class1="bg-yellow-active" class2="bg-yellow" bg3="#222d32"
                                        bg4="#f4f5f7" setSkin={this.setSkin.bind(this)}>Yellow</IconLayout>
                                    <IconLayout skin="blue-light" bg1="#367fa9" class2="bg-light-blue" bg3="#f9fafc"
                                        bg4="#f4f5f7"
                                        setSkin={this.setSkin.bind(this)}>Blue Light</IconLayout>
                                    <IconLayout skin="black-light" bg1="#fefefe" bg2="#fefefe" bg3="#f9fafc"
                                        bg4="#f4f5f7"
                                        blacked={true} setSkin={this.setSkin.bind(this)}>Black Light</IconLayout>
                                    <IconLayout skin="purple-light" class1="bg-purple-active" class2="bg-purple"
                                        bg3="#f9fafc"
                                        bg4="#f4f5f7" setSkin={this.setSkin.bind(this)}>Purple Light</IconLayout>
                                    <IconLayout skin="green-light" class1="bg-green-active" class2="bg-green"
                                        bg3="#f9fafc"
                                        bg4="#f4f5f7" setSkin={this.setSkin.bind(this)}>Green Light</IconLayout>
                                    <IconLayout skin="red-light" class1="bg-red-active" class2="bg-red" bg3="#f9fafc"
                                        bg4="#f4f5f7"
                                        setSkin={this.setSkin.bind(this)}>Red Light</IconLayout>
                                    <IconLayout skin="yellow-light" class1="bg-yellow-active" class2="bg-yellow"
                                        bg3="#f9fafc"
                                        bg4="#f4f5f7" setSkin={this.setSkin.bind(this)}>Yellow Light</IconLayout>
                                </ul>
                            </Box>
                        </ContentColumn>
                        <ContentColumn mdSize={6} xsSize={12}>
                            <Box skin="primary" title="Layout">
                                <FormControl label="Layout" horizontal={false}>
                                    <Select name="layout" handleChange={this.handleChange.bind(this)}
                                        value={this.props.layout} emptyOption={false}>
                                        <option value="default">Default</option>
                                        <option value="fixed">Fixed</option>
                                        <option value="boxed">Boxed</option>
                                        <option value="top-navigation">Top Navigation</option>
                                    </Select>
                                </FormControl>
                                <FormControl label="Sidebar" horizontal={false}>
                                    <Select name="sidebar" handleChange={this.handleChange.bind(this)}
                                        value={this.props.sidebar} emptyOption={false}>
                                        <option value="none">None</option>
                                        <option value="full">Full</option>
                                        <option value="collapsed">Collapsed</option>
                                    </Select>
                                </FormControl>
                                <FormControl label="Collapse Sidebar size" horizontal={false}>
                                    <Select name="miniSidebar" handleChange={this.handleChange.bind(this)}
                                        value={this.props.miniSidebar} emptyOption={false}>
                                        <option value="none">None</option>
                                        <option value="mini">Mini</option>
                                    </Select>
                                </FormControl>
                                <FormControl label="Collapsable Sidebar" horizontal={false}>
                                    <Select name="collapsableSidebar" handleChange={this.handleChange.bind(this)}
                                        value={this.props.collapsableSidebar} emptyOption={false}>
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    </Select>
                                </FormControl>
                                <FormControl label="Controlbar" horizontal={false}>
                                    <Select name="controlbar" handleChange={this.handleChange.bind(this)}
                                        value={this.props.controlbar} emptyOption={false}>
                                        <option value="none">None</option>
                                        <option value="hidden">Hidden</option>
                                        <option value="shown">Shown</option>
                                    </Select>
                                </FormControl>
                                <FormControl label="Controlbar Position" horizontal={false}>
                                    <Select name="controlbarPosition" handleChange={this.handleChange.bind(this)}
                                        value={this.props.controlbarPosition} emptyOption={false}>
                                        <option value="over">Over</option>
                                        <option value="side">Side</option>
                                    </Select>
                                </FormControl>
                                <FormControl label="Controlbar Color" horizontal={false}>
                                    <Select name="controlbarColor" handleChange={this.handleChange.bind(this)}
                                        value={this.props.controlbarColor} emptyOption={false}>
                                        <option value="dark">Dark</option>
                                        <option value="light">Light</option>
                                    </Select>
                                </FormControl>
                            </Box>
                        </ContentColumn>
                    </ContentRow>
                    <ContentRow>
                        <ContentColumn xsSize={12}>
                            <Box title="Code">
                                <PrismCode language="jsx" component="pre">
                                    {generateCode(imports, [MyComponent], this.props)}
                                </PrismCode>
                            </Box>
                        </ContentColumn>
                    </ContentRow>
                </Content>
            </div>
        );
    }
}

function mapStateToProps(state) {

    const {AdminLayoutReducer} = state;

    const {
        skin,
        layout,
        sidebar,
        controlbar,
        controlbarPosition,
        controlbarColor,
        miniSidebar,
        collapsableSidebar,
    } = AdminLayoutReducer;

    return {
        skin,
        layout,
        sidebar,
        controlbar,
        controlbarPosition,
        controlbarColor,
        miniSidebar,
        collapsableSidebar,
    };
}

export default connect(mapStateToProps)(AdminLayoutGenerator);
