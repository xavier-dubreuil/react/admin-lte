import React from 'react';
import {LoginLayout, LoginBox, LoginBoxBody, LoginBoxHeader, LoginBoxMessage} from '../../../lib/index';

export const Login = function () {
    return (
        <LoginLayout>
            <LoginBox>
                <LoginBoxHeader>Admin LTE</LoginBoxHeader>
                <LoginBoxBody>
                    <LoginBoxMessage className="login-box-msg">Sign in to start your session</LoginBoxMessage>
                    <div className="form-group has-feedback">
                        <input type="text" className="form-control" placeholder="Username" name="username"/>
                        <span className="glyphicon glyphicon-envelope form-control-feedback"/>
                    </div>
                    <div className="form-group has-feedback">
                        <input type="password" className="form-control" placeholder="Password" name="password"/>
                        <span className="glyphicon glyphicon-lock form-control-feedback"/>
                    </div>
                    <div className="row">
                        <div className="col-xs-8"/>
                        <div className="col-xs-4">
                            <button type="submit" className="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div>
                    </div>
                </LoginBoxBody>
            </LoginBox>
        </LoginLayout>
    );
};
