import React from 'react';
import {Content} from '../../lib/content/content';
import {ContentHeader} from '../../lib/content/content-header';
import {ContentRow} from '../../lib/content/content-row';
import {ContentColumn} from '../../lib/content/content-column';

export const Installation = function () {
    return (
        <Content>
            <ContentHeader title="Installation" />
            <ContentRow>
                <ContentColumn xsSize={12}>

                </ContentColumn>
            </ContentRow>
        </Content>
    );
};