import React from 'react';
import {DemoGenerator, generateAttributes, generateIcon} from '../demo/demo';
import {
    InfoBox,
    InfoBoxText,
    InfoBoxNumber,
    ProgressBar,
    ProgressDescription,
    FormControl,
    Input,
    Select,
    Colors,
} from '../../../lib/index';

const imports = [
    {
        from: 'react-xd-admin-lte',
        imports: ['ContentRow', 'ContentColumn', 'InfoBox', 'InfoBoxText', 'InfoBoxNumber', 'ProgressBar', 'ProgressDescription']
    },
];

const state = {
    color: '',
    iconType: 'fa',
    iconName: 'envelope-o',
    iconColor: 'aqua',
    isLoading: false,
    text: 'MESSAGES',
    number: '2,456',
    percent: 0,
    description: '',
};

const paramsOptions = function (props) {
    return (
        <div>
            <FormControl label="Box Color" horizontal={false}>
                <Select name="color" handleChange={props.handleChange}
                    value={props.state.color} emptyOption={false}>
                    <option value="">Default</option>
                    {Colors.map((color) => (
                        <option key={color} value={color}>{color}</option>
                    ))}
                </Select>
            </FormControl>
            <FormControl label="Icon Color" horizontal={false}>
                <Select name="iconColor" handleChange={props.handleChange}
                    value={props.state.iconColor} emptyOption={false}>
                    <option value="">Default</option>
                    {Colors.map((color) => (
                        <option key={color} value={color}>{color}</option>
                    ))}
                </Select>
            </FormControl>
            <FormControl label="Icon Type" horizontal={false}>
                <Select name="iconType" handleChange={props.handleChange}
                    value={props.state.iconType}>
                    <option value="">No Icon</option>
                    <option value="fa">Font Awesome</option>
                    <option value="glyph">Glyph Icons</option>
                    <option value="ion">Ion Icons</option>
                </Select>
            </FormControl>
            <FormControl label="Icon Name" horizontal={false}>
                <Input type="text" name="iconName" handleChange={props.handleChange}
                    value={props.state.iconName} />
            </FormControl>
            <FormControl label="Text" horizontal={false}>
                <Input type="text" name="text" handleChange={props.handleChange}
                    value={props.state.text} />
            </FormControl>
            <FormControl label="Number" horizontal={false}>
                <Input type="text" name="number" handleChange={props.handleChange}
                    value={props.state.number} />
            </FormControl>
            <FormControl label="Percent" horizontal={false}>
                <Input type="number" name="percent" handleChange={props.handleChange}
                    value={props.state.percent} />
            </FormControl>
            <FormControl label="Description" horizontal={false}>
                <Input type="text" name="description" handleChange={props.handleChange}
                    value={props.state.description} />
            </FormControl>
        </div>
    );
};

const myComponent = function (props) {
    let demoAttributes = generateAttributes(props, ['color', 'iconColor', 'isLoading']);
    demoAttributes['icon'] = generateIcon(props.iconType, props.iconName);

    return (
        <InfoBox {...demoAttributes}>
            <InfoBoxText>{props.text}</InfoBoxText>
            <InfoBoxNumber>{props.number}</InfoBoxNumber>
            {props.percent > 0 &&
            <ProgressBar percent={props.percent} />
            }
            {props.description !== '' &&
            <ProgressDescription>{props.description}</ProgressDescription>
            }
        </InfoBox>
    );
};

export const InfoboxGenerator = function () {
    return (
        <DemoGenerator state={state} component={myComponent} imports={imports} api={[InfoBox, InfoBoxText, InfoBoxText]}
            params={paramsOptions} />
    );
};