import React from 'react';
import {DemoGenerator, generateAttributes, generateIcon} from '../demo/demo';
import {
    SmallBox,
    Icon,
    FormControl,
    Input,
    Select,
    Colors,
} from '../../../lib/index';

const imports = [
    {
        from: 'react-xd-admin-lte',
        imports: ['ContentRow', 'ContentColumn', 'InfoBox', 'InfoBoxText', 'InfoBoxNumber', 'ProgressBar', 'ProgressDescription']
    },
];

const state = {
    color: 'aqua',
    iconType: 'fa',
    iconName: 'envelope-o',
    title: '150',
    text: 'MESSAGES',
    to: '#',
};

const paramsOptions = function (props) {
    return (
        <div>
            <FormControl label="Box Color" horizontal={false}>
                <Select name="color" handleChange={props.handleChange}
                    value={props.state.color} emptyOption={false}>
                    <option value="">Default</option>
                    {Colors.map((color) => (
                        <option key={color} value={color}>{color}</option>
                    ))}
                </Select>
            </FormControl>
            <FormControl label="Icon Type" horizontal={false}>
                <Select name="iconType" handleChange={props.handleChange}
                    value={props.state.iconType}>
                    <option value="">No Icon</option>
                    <option value="fa">Font Awesome</option>
                    <option value="glyph">Glyph Icons</option>
                    <option value="ion">Ion Icons</option>
                </Select>
            </FormControl>
            <FormControl label="Icon Name" horizontal={false}>
                <Input type="text" name="iconName" handleChange={props.handleChange}
                    value={props.state.iconName} />
            </FormControl>
            <FormControl label="Title" horizontal={false}>
                <Input type="text" name="title" handleChange={props.handleChange}
                    value={props.state.title} />
            </FormControl>
            <FormControl label="Text" horizontal={false}>
                <Input type="text" name="text" handleChange={props.handleChange}
                    value={props.state.text} />
            </FormControl>
        </div>
    );
};

const Footer = function (props) {
    return (
        <span>
            {props.text}
            <Icon tag="i" type="fa" name="arrow-circle-right" />
        </span>
    );
};

const myComponent = function (props) {
    let demoAttributes = generateAttributes(props, ['color', 'to']);
    demoAttributes['icon'] = generateIcon(props.iconType, props.iconName);
    demoAttributes['footer'] = <Footer text="More info" />;

    return (
        <SmallBox {...demoAttributes}>
            <h3>{props.title}</h3>
            <p>{props.text}</p>
        </SmallBox>
    );
};

export const SmallboxGenerator = function () {
    return (
        <DemoGenerator state={state} component={myComponent} additionalComponents={[Footer]} imports={imports}
            api={[SmallBox]} params={paramsOptions} />
    );
};
