import React from 'react';
import {DemoGenerator, generateAttributes} from '../demo/demo';
import {
    Box,
    FormControl,
    Input,
    Select,
    Skins,
} from '../../../lib/index';

const imports = [
    {
        from: 'react-xd-admin-lte',
        imports: ['ContentRow', 'ContentColumn', 'Box', 'InfoBoxText', 'InfoBoxNumber', 'ProgressBar', 'ProgressDescription']
    },
];

const state = {
    skin: 'primary',
    header: null,
    title: 'Test Box',
    titleTools: null,
    loading: false,
    solid: false,
    textCenter: false,
    text: 'Body of the box !Body of the box !Body of the box !Body of the box !Body of the box !Body of the box !Body of the box !',
    footer: 'Footer of the box',
    collapsable: false,
    collapsed: false,
    closable: false,
};

const paramsOptions = function (props) {

    const {handleChange} = props;
    return (
        <div>
            <FormControl label="Box Skin" horizontal={false}>
                <Select name="skin" handleChange={props.handleChange} value={props.state.skin} emptyOption={false}>
                    {Skins.map((skin) => (
                        <option key={skin} value={skin}>{skin}</option>
                    ))}
                </Select>
            </FormControl>
            <FormControl label="Is Loading" horizontal={false}>
                <Input type="checkbox" name="loading" handleChange={handleChange} value={props.state.loading}/>
            </FormControl>
            <FormControl label="Solid Box" horizontal={false}>
                <Input type="checkbox" name="solid" handleChange={handleChange} value={props.state.solid}/>
            </FormControl>
            <FormControl label="Collapsable" horizontal={false}>
                <Input type="checkbox" name="collapsable" handleChange={handleChange} value={props.state.collapsable}/>
            </FormControl>
            <FormControl label="Collapsed" horizontal={false}>
                <Input type="checkbox" name="collapsed" handleChange={handleChange} value={props.state.collapsed}/>
            </FormControl>
            <FormControl label="Closable" horizontal={false}>
                <Input type="checkbox" name="closable" handleChange={handleChange} value={props.state.closable}/>
            </FormControl>
            <FormControl label="Text Center" horizontal={false}>
                <Input type="checkbox" name="textCenter" handleChange={handleChange} value={props.state.textCenter}/>
            </FormControl>
            <FormControl label="Title" horizontal={false}>
                <Input type="text" name="title" handleChange={handleChange} value={props.state.title}/>
            </FormControl>
            <FormControl label="Text" horizontal={false}>
                <Input type="text" name="text" handleChange={handleChange} value={props.state.text}/>
            </FormControl>
            <FormControl label="Text" horizontal={false}>
                <Input type="text" name="footer" handleChange={handleChange} value={props.state.footer}/>
            </FormControl>
        </div>
    );
};


const myComponent = function (props) {
    const list = ['skin', 'loading', 'solid', 'title', 'footer', 'collapsable', 'collapsed', 'closable', 'textCenter'];
    const demoAttributes = generateAttributes(props, list);

    return (
        <Box {...demoAttributes}>
            <p>{props.text}</p>
        </Box>
    );
};

export const BoxGenerator =function() {
    return (
        <DemoGenerator state={state} component={myComponent} imports={imports} api={[Box]} params={paramsOptions}/>
    );
};