import React from 'react';
import FontAwesome from 'react-fontawesome';
import {NavbarMenu, NavbarMenuItem, NavbarMenuDivider} from '../lib';

export const Navbar = function (props) {
    return (
        <div>
            <NavbarMenu>
                <NavbarMenuItem title="Alexander Pierce">
                    <NavbarMenuItem title="Profile"/>
                    <NavbarMenuDivider/>
                    <NavbarMenuItem title="Signout"/>
                </NavbarMenuItem>
                {props.toggleControlbar &&
                <NavbarMenuItem title={<FontAwesome name="gear" />} onClick={() => {
                    props.toggleControlbar();
                }} />
                }
            </NavbarMenu>
        </div>
    );
};
