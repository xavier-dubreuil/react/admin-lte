// React
import React, {Component} from 'react';
import {Provider} from 'react-redux';
import configureStore from './configureStore';
import {Router, Switch, Route} from 'react-router-dom';
import createHashHistory from 'history/createHashHistory';

// Reducers
import AdminLayoutReducer from './reducers/admin-layout-reducer';

// Components
import {Login} from './components/examples/login';
import Application from './application';

const initialState = {
    AdminLayoutReducer: {
        skin: 'blue',
        layout: 'default',
        sidebar: 'full',
        controlbar: 'hidden',
        controlbarPosition: 'over',
        controlbarColor: 'black',
        miniSidebar: 'mini',
        collapsableSidebar: 'yes',
    }
};

const store = configureStore({AdminLayoutReducer}, initialState, false);

const history = createHashHistory();

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router history={history}>
                    <Switch>
                        <Route path="/login" component={Login} />
                        <Route path="/" component={Application} />
                    </Switch>
                </Router>
            </Provider>
        );
    }
}

export default App;
