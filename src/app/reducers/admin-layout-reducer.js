import {
    CHANGE_ADMIN_LAYOUT_PARAMETER,
} from '../actions/admin-layout-actions';

function reducer(state = {}, action) {
    switch (action.type) {
    case CHANGE_ADMIN_LAYOUT_PARAMETER:
        return Object.assign({}, state, {
            [action.param]: action.value,
        });
    default:
        return state;
    }
}

function AdminLayoutReducer(state = {}, action) {
    switch (action.type) {
    case CHANGE_ADMIN_LAYOUT_PARAMETER:
        return Object.assign({}, state, reducer(state, action));
    default:
        return state;
    }
}

export default AdminLayoutReducer;
