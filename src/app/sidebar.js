import React from 'react';
import {SidebarMenu, SidebarMenuHeader, SidebarMenuItem} from '../lib';

export const Sidebar = function () {
    return (
        <SidebarMenu>
            <SidebarMenuHeader title="COMPONENTS" />
            <SidebarMenuItem title="Layout" icon="th">
                <SidebarMenuItem title="Admin" to="/layout/admin" />
                <SidebarMenuItem title="Login Box" to="/layout/login" labels={[{title: 'todo', color: 'yellow'}]} />
            </SidebarMenuItem>
            <SidebarMenuItem title="Boxes" icon="th">
                <SidebarMenuItem title="Info Box" to="/boxes/info" />
                <SidebarMenuItem title="Small Box" to="/boxes/small" />
                <SidebarMenuItem title="Box" to="/boxes/box" />
            </SidebarMenuItem>
            <SidebarMenuItem title="UI Elements" icon="laptop">
                <SidebarMenuItem title="Alert" to="/ui-elements/alert" />
                <SidebarMenuItem title="Callout" to="/ui-elements/callout" />
                <SidebarMenuItem title="Progress Bar" to="/ui-elements/progress-bar" />
                <SidebarMenuItem title="Icons" to="/ui-elements/icon" labels={[{title: 'todo', color: 'yellow'}]} />
            </SidebarMenuItem>
            <SidebarMenuItem title="Forms" icon="edit">
                <SidebarMenuItem title="Input" to="/forms/input" labels={[{title: 'todo', color: 'yellow'}]} />
                <SidebarMenuItem title="Input Group" to="/forms/input-group"
                    labels={[{title: 'todo', color: 'yellow'}]} />
                <SidebarMenuItem title="Checkbox" to="/form/checkbox" labels={[{title: 'todo', color: 'yellow'}]} />
                <SidebarMenuItem title="Select" to="/form/select" labels={[{title: 'todo', color: 'yellow'}]} />
                <SidebarMenuItem title="Textarea" to="/form/textarea" labels={[{title: 'todo', color: 'yellow'}]} />
                <SidebarMenuItem title="Form Control" to="/form/form-control"
                    labels={[{title: 'todo', color: 'yellow'}]} />
            </SidebarMenuItem>
            <SidebarMenuItem title="Tables" icon="table">
                <SidebarMenuItem title="Simple" to="/tables/simple" labels={[{title: 'todo', color: 'yellow'}]} />
                <SidebarMenuItem title="Data" to="/tables/data" labels={[{title: 'todo', color: 'yellow'}]} />
            </SidebarMenuItem>
        </SidebarMenu>

    );
};
