import React from 'react';
import {connect} from 'react-redux';
import {Switch, Route, Link} from 'react-router-dom';
import {AdminLayout} from '../lib';
import {Navbar} from './navbar';
import {Sidebar} from './sidebar';
import {Controlbar} from './controlbar';

import AdminLayoutGenerator from './components/layout/admin';
import {InfoboxGenerator} from './components/boxes/info-box';
import {SmallboxGenerator} from './components/boxes/small-box';
import {BoxGenerator} from './components/boxes/box';

import {AlertGenerator} from './components/ui-elements/alert';
import {CalloutGenerator} from './components/ui-elements/callout';
import {ProgressBarGenerator} from './components/ui-elements/progress-bar';
import {ButtonGenerator} from './components/ui-elements/button';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'admin-lte/dist/css/AdminLTE.min.css';
import 'admin-lte/dist/css/skins/_all-skins.min.css';
import 'font-awesome/css/font-awesome.min.css';
import 'ionicons/dist/css/ionicons.min.css';
import './application.css';
import {Installation} from './components/installation';
import {NavbarMenu} from '../lib/navbar/navbar-menu';
import {NavbarMenuItem} from '../lib/navbar/navbar-menu-item';

const navigation = function () {
    return (
        <NavbarMenu>
            <NavbarMenuItem title="Layout">
                <li><Link to="/layout/admin">Admin</Link></li>
                <li><Link to="/layout/login">Login</Link></li>
            </NavbarMenuItem>
        </NavbarMenu>
    );
};

class Application extends React.Component {

    render() {
        let attributes = {
            logoMini: 'ALTE',
            logoLarge: 'Admin LTE',
            layout: this.props.layout,
            skin: this.props.skin,
            navbar: Navbar,
            collapsedSidebar: this.props.sidebar === 'collapsed',
            showControlbar: this.props.controlbar === 'shown',
            sideControlbar: this.props.controlbarPosition === 'side',
            lightControlbar: this.props.controlbarColor === 'light',
            navigation: navigation,
            miniSidebar: this.props.miniSidebar === 'mini',
            collapsableSidebar: this.props.collapsableSidebar === 'yes',
        };
        if (this.props.sidebar !== 'none') {
            attributes['sidebar'] = <Sidebar />;
        }
        if (this.props.sidebar !== 'none') {
            attributes['controlbar'] = <Controlbar />;
        }

        return (
            <AdminLayout {...attributes}>
                <Switch>
                    <Route exact path="/installation" component={Installation} />
                    <Route exact path="/layout/admin" component={AdminLayoutGenerator} />

                    <Route exact path="/boxes/info" component={InfoboxGenerator} />
                    <Route exact path="/boxes/small" component={SmallboxGenerator} />
                    <Route exact path="/boxes/box" component={BoxGenerator} />

                    <Route exact path="/ui-elements/alert" component={AlertGenerator} />
                    <Route exact path="/ui-elements/callout" component={CalloutGenerator} />
                    <Route exact path="/ui-elements/progress-bar" component={ProgressBarGenerator} />
                    <Route exact path="/ui-elements/button" component={ButtonGenerator} />
                </Switch>
            </AdminLayout>
        );
    }

}

function mapStateToProps(state) {

    const {AdminLayoutReducer} = state;

    const {
        skin,
        layout,
        sidebar,
        controlbar,
        controlbarPosition,
        controlbarColor,
        miniSidebar,
        collapsableSidebar,
    } = AdminLayoutReducer;

    return {
        skin,
        layout,
        sidebar,
        controlbar,
        controlbarPosition,
        controlbarColor,
        miniSidebar,
        collapsableSidebar,
    };
}

export default connect(mapStateToProps)(Application);
