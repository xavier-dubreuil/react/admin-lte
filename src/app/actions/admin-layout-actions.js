export const CHANGE_ADMIN_LAYOUT_PARAMETER = 'CHANGE_ADMIN_LAYOUT_PARAMETER';

function _changeParameter(param, value) {
    return {
        type: CHANGE_ADMIN_LAYOUT_PARAMETER,
        param: param,
        value: value,
    };
}

export function changeParameter(param, value) {
    return dispatch => {
        dispatch(_changeParameter(param, value));
    };
}
