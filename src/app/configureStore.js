import {createStore, applyMiddleware} from 'redux';
import {combineReducers} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger';

export default function configureStore(reducers, preloadedState, reduxLogger = false) {

    const rootReducer = combineReducers({
        ...reducers
    });

    let middlewares = [];
    middlewares.push(thunkMiddleware);

    if (reduxLogger) {
        middlewares.push(createLogger());
    }

    return createStore(
        rootReducer,
        preloadedState,
        applyMiddleware(...middlewares)
    );
}
